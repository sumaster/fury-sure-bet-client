import {Injectable} from '@angular/core';
import {SureBetList} from "../../tables/sure-bet-table/model/sure-bet-list.model";
import {timer} from "rxjs/internal/observable/timer";
import {concatMap, map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {BookmakerDict} from "../../tables/sure-bet-table/model/bookmaker-dict";
import {SportDict} from "../../tables/sure-bet-table/model/sport-dict";
import {PeriodDict} from '../../tables/sure-bet-table/model/period-dict';

@Injectable()
export class DataApi {
  errorMessage: string;

  dataPipe$: Observable<SureBetList[]>;
  bookmakerDictPipe$: Observable<BookmakerDict[]>;
  sportDictPipe$: Observable<SportDict[]>;
  periodDictPipe$: Observable<PeriodDict[]>;


  constructor(private http: HttpClient) {
    this.dataLoader();
    this.bookmakerDictionaryLoader();
    this.sportDictionaryLoader();

  }

  private dataLoader() {
    this.dataPipe$ = timer(0, 3000)
      .pipe(concatMap(_ => this.http.get<SureBetList[]>('/assets/data/dump3-170-forks.json')))
      .pipe(map(source => {
        //эмуляция обновления списка
        let startPoint = Math.floor(Math.random() * source.length / 2);
        let sliceLength = Math.floor(Math.random() * (source.length - startPoint - 20)) + 20;
        console.log(startPoint, sliceLength);
        return source.map(item => new SureBetList(item)).slice(startPoint, sliceLength);
      }));
  }

  getData() {
    return this.dataPipe$;
  }

  private bookmakerDictionaryLoader() {
    this.bookmakerDictPipe$ = timer(0, 160000)
      .pipe(concatMap(_ => this.http.get<BookmakerDict[]>('/assets/data/bookmakerDictionary.json')))
      .pipe(map(source => source.map(item => new BookmakerDict(item))));
  }

  getBookersDict() {
    return this.bookmakerDictPipe$;
  }

  private sportDictionaryLoader() {
    this.sportDictPipe$ = timer(0, 160000)
      .pipe(concatMap(_ => this.http.get<SportDict[]>('/assets/data/sportDictionary.json')))
      .pipe(map(source => source.map(item => new SportDict(item))));
  }

  getSportDict() {
    return this.sportDictPipe$;
  }

  private periodDictionaryLoader() {
    this.periodDictPipe$ = timer(0, 160000)
      .pipe(concatMap(_ => this.http.get<PeriodDict[]>('/assets/data/periodDictionary.json')))
      .pipe(map(source => source.map(item => new PeriodDict(item))));
  }

  getPeriodDict() {
    return this.periodDictPipe$;
  }
}
