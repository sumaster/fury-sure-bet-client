import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BreadcrumbsModule} from '../../../../@fury/shared/breadcrumbs/breadcrumbs.module';
import {ListModule} from '../../../../@fury/shared/list/list.module';
import {MaterialModule} from '../../../../@fury/shared/material-components.module';
import {SureBetTableRoutingModule} from './sure-bet-table-routing.module';
import {SureBetTableComponent} from './sure-bet-table.component';
import {FurySharedModule} from '../../../../@fury/fury-shared.module';
import {DataApi} from '../../components/data-api/data-api';

@NgModule({
    imports: [
        CommonModule,
        SureBetTableRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FurySharedModule,


        // Core
        ListModule,
        BreadcrumbsModule,

        //WKL
        ListModule
    ],
    declarations: [SureBetTableComponent],
    exports: [SureBetTableComponent],
    providers: [DataApi]
})
export class SureBetTableModule {
}
