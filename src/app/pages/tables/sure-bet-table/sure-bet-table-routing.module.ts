import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SureBetTableComponent } from './sure-bet-table.component';

const routes: Routes = [
  {
    path: '',
    component: SureBetTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SureBetTableRoutingModule {
}
