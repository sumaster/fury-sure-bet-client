export class BookmakerDict {
  id: number;

  name: string;

  constructor(dictObj) {
    this.id = dictObj.id;

    this.name = dictObj.name;
  }
}
