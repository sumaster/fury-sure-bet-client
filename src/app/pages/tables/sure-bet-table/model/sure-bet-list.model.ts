import {OddItem} from "./odd-item.model";

export class SureBetList {
  id: number;

  aggregatedEventId: number;

  percent: number;

  age: number;

  sportId: number;

  periodId: number;

  status: string;

  forkTypeId: number;

  outcomes: Array<OddItem>;

  // updated: number;
  //
  // updatedStr: string;
  //
  // changed: number;

  eventLeagueName: string;

  timeNow: number = Date.now();

  constructor(SureBetObj) {
    this.id = SureBetObj.id;

    this.aggregatedEventId = SureBetObj.aggregatedEventId;

    this.percent = SureBetObj.percent;

    this.age = SureBetObj.age;

    this.sportId = SureBetObj.sportId;

    this.periodId = SureBetObj.periodId;

    this.status = SureBetObj.status;

    this.forkTypeId = SureBetObj.forkTypeId;

    this.outcomes = SureBetObj.outcomes;

    this.eventLeagueName = SureBetObj.outcomes[0].event + " " + SureBetObj.outcomes[0].league;

  }

  /* Virtual attributes.
  *  Which does not come from JSON API.
  *  We compute them at this client for filter and sorting purpose
  * */

  // get eventLeagueName() {
  //   return this.outcomes[0].event + " " + this.outcomes[0].league;
  // }

  get changed() {
    return this.timeNow - Math.max.apply(null, this.outcomes.map((outcome) => outcome.changedAt))
  }

  get updated() {
    return this.timeNow - Math.max.apply(null, this.outcomes.map((outcome) => outcome.updatedAt));
  }
}
