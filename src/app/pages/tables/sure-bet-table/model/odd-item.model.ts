export class OddItem
{
  name: string;

  price: number;

  event: string;

  score: string;

  league: string;

  startTime: number;

  changedAt: number;

  updatedAt: number;

  bookmakerId: number;

  directLink: string;

  status: string;

  dbgMarketLink: string;

  dbgMonitorLink: string;

  constructor(oddObj) {
    this.name = oddObj.name;

    this.price = oddObj.price;

    this.event = oddObj.event;

    this.score = oddObj.score;

    this.league = oddObj.league;

    this.startTime = oddObj.startTime;

    this.changedAt = oddObj.changedAt;

    this.updatedAt = oddObj.updatedAt;

    this.bookmakerId = oddObj.bookmakerId;

    this.directLink = oddObj.directLink;

    this.status = oddObj.status;

    this.dbgMarketLink = oddObj.dbgMarketLink;

    this.dbgMonitorLink = oddObj.dbgMonitorLink;
  }
}
