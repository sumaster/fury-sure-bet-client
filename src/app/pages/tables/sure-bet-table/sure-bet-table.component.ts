import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Observable, ReplaySubject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {ListColumn} from '../../../../@fury/shared/list/list-column.model';
import {SureBetList} from './model/sure-bet-list.model';
import {fadeInRightAnimation} from '../../../../@fury/animations/fade-in-right.animation';
import {fadeInUpAnimation} from '../../../../@fury/animations/fade-in-up.animation';
import {MatSnackBar} from '@angular/material/snack-bar';
import {StorageMap} from '@ngx-pwa/local-storage';
import {HttpClient} from '@angular/common/http';
import {DataApi} from '../../components/data-api/data-api';
import {BookmakerDict} from './model/bookmaker-dict';


@Component({
  selector: 'fury-all-in-one-table',
  templateUrl: './sure-bet-table.component.html',
  styleUrls: ['./sure-bet-table.component.scss'],
  animations: [fadeInRightAnimation, fadeInUpAnimation]
})

export class SureBetTableComponent implements OnInit, AfterViewInit, OnDestroy {

  /**
   * Simulating a service with HTTP that returns Observables
   * You probably want to remove this and do all requests in a service with HTTP
   */
  subject$: ReplaySubject<SureBetList[]> = new ReplaySubject<SureBetList[]>(1);
  data$: Observable<SureBetList[]> = this.subject$.asObservable();


  @Input()
  columns: ListColumn[] = [
    {name: 'Percent', property: 'percent', visible: true, isModelProperty: false},
    {name: 'SportId', property: 'sportId', visible: true, isModelProperty: true},
    {name: 'PeriodId', property: 'periodId', visible: true, isModelProperty: true},
    {name: 'EventLeagueName', property: 'eventLeagueName', visible: true, isModelProperty: true},
    {name: 'Status', property: 'status', visible: true, isModelProperty: true},
    {name: 'ForkTypeId', property: 'forkTypeId', visible: true, isModelProperty: true},
    {name: 'Updated', property: 'updated', visible: true, isModelProperty: true},
    {name: 'Changed', property: 'changed', visible: true, isModelProperty: true},
    {name: 'Debug', property: 'debug', visible: true, isModelProperty: false}
  ] as ListColumn[];
  pageSize = 10;
  dataSource: MatTableDataSource<SureBetList> | null;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private dialog: MatDialog, private snackBar: MatSnackBar, private storage: StorageMap, private http: HttpClient, private dataApi: DataApi) {
    this.initColumnVisibility();
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  openSnackbar(text: string) {
    this.snackBar.open(text, 'CLOSE', {
      duration: 3000,
      horizontalPosition: 'left'
    });
  }


  openLink(link) {
    window.open(link, '_blank');
  }

  copyToClipboard(inputElement: HTMLInputElement) {
    inputElement.focus();
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.openSnackbar('COPIED: ' + inputElement.value);
  }

  openDialog() {

  }

  deleteRow(customer) {
    /**
     * Here we are updating our local array.
     * You would probably make an HTTP request here.
     */
    // this.customers.splice(this.customers.findIndex((existingCustomer) => existingCustomer.id === customer.id), 1);
    // this.subject$.next(this.customers);
  }

  initColumnVisibility() {
    // this.storage.keys().subscribe({
    //     next: (key) => {
    //         console.log(key);
    //     },
    //     complete: () => {
    //         console.log('Done');
    //     },
    // });

    var that = this;
    this.columns.forEach(function (column) {
      var key = 'visibleColumns_' + column.name;
      that.storage.get(key).subscribe((columnVisable) => {
        column.visible = columnVisable == false ? false : column.visible;
        //console.log("initColumnVisibility:: " + key + " : " + columnVisable + " " + column.visible);
      });
    })
  }

  ngOnInit() {
    this.dataApi.getData().subscribe(sureBetItem => {
      this.subject$.next(sureBetItem);
    });

    this.dataSource = new MatTableDataSource();

    this.data$.pipe(
      filter(Boolean)
    ).subscribe((sureBetList) => {
      this.dataSource.data = sureBetList;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onFilterChange(value) {
    if (!this.dataSource) {
      return;
    }

    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  ngOnDestroy() {
  }

  openBugReport() {

  }


  getBookmakerById(id: number)
  {

  }
}
